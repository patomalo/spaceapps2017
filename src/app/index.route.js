(function() {
  'use strict';

  angular
    .module('bc')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('charts', {
        url: '/charts',
        templateUrl: 'app/components/charts/chart.html',
        controller: 'ChartController',
        controllerAs: 'vm'
      })
      .state('map1', {
        url: '/map1',
        templateUrl: 'app/components/map/map1/map1.html',
        controller: 'Map1Controller',
        controllerAs: 'map1'
      })
      .state('map2', {
        url: '/map2',
        templateUrl: 'app/components/map/map2/map2.html',
        controller: 'Map2Controller',
        controllerAs: 'map1'
      })
      .state('disasterList', {
        url: '/disasterList',
        templateUrl: 'app/components/disasters/disasters.html',
        controller: 'DisastersListController',
        controllerAs: 'dis1'
      })
      .state('reports', {
        url: '/reports',
        templateUrl: 'app/components/reports/report.html',
        controller: 'ReportController',
        controllerAs: 'dis1'
      })
      .state('disasterReport', {
      url: '/disasterReport',
      templateUrl: 'app/components/disasters/report.dis.html',
      controller: 'DisastersReportController',
      controllerAs: 'vm'
    });
    
    $urlRouterProvider.otherwise('/');
  }

})();
