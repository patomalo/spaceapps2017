angular
  .module('bc')
  .controller('Map2Controller', Map2Controller);

function Map2Controller($scope) {
  var map2 = this;

  map2.countryList = [{
    name: "Bolivia", code: "BO"
  }, {
    name: "Estados Unidos", code: "US"
  }];

  map2.regionList = [{
    name: "La Paz", code: "LP"
  }, {
    name: "Cochabamba", code: "CB"
  }, {
    name: "Santa Cruz", code: "SC"
  }, {
    name: "Oruro", code: "OR"
  }, {
    name: "Potosi", code: "PT"
  }, {
    name: "Tarija", code: "TR"
  }, {
    name: "Trinidad", code: "TI"
  }, {
    name: "Cobija", code: "CO"
  }, {
    name: "Sucre", code: "SU"
  }, {
    name: "El Alto", code: "EA"
  }];
  map2.regionListUs = [{
    name: "Los Angeles", code: "LA"
  }, {
    name: "New York", code: "NY"
  }];
  map2.regionSelected = null;
  map2.countryFlag = null;

  map2.changeCountry = function (code) {
    map2.countryFlag = code;
  }

  map2.regionsListsDetails = {
    LP: {
      long: -68.6779751, lat: -16.5558537, zoom: 15
    },
    CB: {
      long: -66.1814741, lat: -17.3888048, zoom: 15
    },
    SC: {
      long: -63.1924458, lat: -17.7952044, zoom: 15
    },
    OR: {
      long: -67.112821, lat: -17.9667513, zoom: 15
    },
    PT: {
      long: -65.7647938, lat: -19.5725897, zoom: 15
    },
    TR: {
      long: -64.7331279, lat: -21.5254445, zoom: 15
    },
    TI: {
      long: -64.9169532, lat: -14.8312283, zoom: 14
    },
    CO: {
      long: -68.776051, lat: -11.0223815, zoom: 15
    },
    SU: {
      long: -65.2641298, lat: -19.0380012, zoom: 15
    },
    EA: {
      long: -68.2087147, lat: -16.5136118, zoom: 14
    },
    BO: {
      long: -65.0528919, lat: -16.6162829, zoom: 6
    },
    US: {
      long: -110.0986889, lat: 39.6715364, zoom: 5
    },
    LA: {
      long: -118.6919284, lat: 34.0201812, zoom: 10
    },
    NY: {
      long: -74.0392714, lat: 40.7590403, zoom: 12
    }
  };

  function centerMap(long, lat, zoom) {
    console.log("Long: " + long + " Lat: " + lat);
    map.getView().setCenter(ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'));
    map.getView().setZoom(zoom);
  }

  map2.changedSelect = function(value){
    if(value) {
      centerMap(map2.regionsListsDetails[value].long, map2.regionsListsDetails[value].lat, map2.regionsListsDetails[value].zoom);
    }
  };

}
