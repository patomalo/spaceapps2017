/**
 * Created by apatty-laptop on 4/29/2017.
 */
(function () {
  'use strict';

  angular
    .module('bc')
    .controller('DisastersReportController', DisastersReportController);

  function DisastersReportController($http, Restangular) {
    var vm = this;
    vm.category = {
      luz: true,
      temp: false,
      gas: false
    };

    vm.regionSelected = "2017";

    vm.chart = null;

    vm.chartConfig = {
      labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo"],
      dataSet: [{
        label: 'Nro. de Desastres',
        lineTension: 0.1,
        data: [4.5, 3.2, 6.5, 1.4, 0.4],
        backgroundColor: 'rgba(63, 127, 191, 0.2)',
        borderColor: 'rgba(63, 127, 191, 0.6)'
      }]
    };

    vm.tableData = {};

    vm.buttons = {
      chartUpdate: chartUpdate
    };

    vm.nodeList = {};
    vm.dataChartList = [];
    vm.dataChartLabel = [];
    vm.dataTableList = [];
    vm.tableHeaders = {};

    function chartUpdate(flag, type) {
      if(flag) {
        console.log(type + ' on');
        //console.log(vm.chart);
      }
      if(flag && type === 'luz') {
        vm.category.temp = false;
        vm.category.gas = false;

        sensorRequest().get().then(function (response) {
          var size = response.data.length;
          createChartData(100, 'luz', response.data, size);
          createTableData(15, 'luz', response.data, size);

          //console.log(vm.dataChartLabel, vm.dataChartList);
          vm.chart.data.labels = vm.dataChartLabel;
          vm.chart.data.datasets = [{
            label: 'Luz',
            lineTension: 0.1,
            data: vm.dataChartList,
            backgroundColor: 'rgba(63, 127, 191, 0.2)',
            borderColor: 'rgba(63, 127, 191, 0.6)',
            pointBorderColor: 'rgba(63, 127, 191, 1)'
          }];
          vm.chart.update();
        }, function () {
          console.log("Request Error");
        });
      }
      if(flag && type === 'temp') {
        vm.category.luz = false;
        vm.category.gas = false;

        sensorRequest().get().then(function (response) {
          var size = response.data.length;
          createChartData(100, 'temp', response.data, size);
          createTableData(15, 'temp', response.data, size);

          //console.log(vm.dataChartLabel, vm.dataChartList);
          vm.chart.data.labels = vm.dataChartLabel;
          vm.chart.data.datasets = [{
            label: 'Temperatura',
            lineTension: 0.1,
            data: vm.dataChartList,
            backgroundColor: 'rgba(19, 96, 0, 0.2)',
            borderColor: 'rgba(19, 96, 0, 0.6)',
            pointBorderColor: 'rgba(19, 96, 0, 1)'
          }];
          vm.chart.update();
        }, function () {
          console.log("Request Error");
        });
      }
      if(flag && type === 'gas') {
        vm.category.luz = false;
        vm.category.temp = false;

        sensorRequest().get().then(function (response) {
          var size = response.data.length;
          createChartData(100, 'gas', response.data, size);
          createTableData(15, 'gas', response.data, size);

          //console.log(vm.dataChartLabel, vm.dataChartList);
          vm.chart.data.labels = vm.dataChartLabel;
          vm.chart.data.datasets = [{
            label: 'Gas',
            lineTension: 0.1,
            data: vm.dataChartList,
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: 'rgba(255,99,132,0.6)',
            pointBorderColor: 'rgba(255,99,132,1)'
          }];
          vm.chart.update();
        }, function () {
          console.log("Request Error");
        });
      }
    }

    function  nodesRequest() {
      //return Restangular.oneUrl('storage/nodes', ' http://1915ac4c.ngrok.io/');
      return Restangular.oneUrl('assets/data/nodes.json');
    }

    function  sensorRequest() {
      //return Restangular.oneUrl('storage/nodes', ' http://ggizitim.enjambre.com.bo/');
      return Restangular.oneUrl('assets/data/sensordata.json');
    }
    function createNodesList(list) {
      for(var k = 0; k< list.length; k++) {
        var node = list[k];
        if(node.nodeID) {
          //vm.nodeList.push(node);
          vm.nodeList[node.nodeID] = node.name;
        }
      }
    }

    function createChartData(count, type, data, size) {
      var typeCategory = '';
      if(type === 'luz') {
        typeCategory = 'lux';
      }
      if(type === 'temp') {
        typeCategory = 'temp';
      }
      if(type === 'gas') {
        typeCategory = 'gas';
      }
      vm.dataChartList = [];
      vm.dataChartLabel = [];
      for(var j = size - count; j < size; j++) {
        vm.dataChartList.push(data[j][typeCategory]);
        vm.dataChartLabel.push(data[j].nodeID);
      }
      //console.log(vm.dataChartList, vm.dataChartLabel);
    }

    function createTableData(count, type, data, size) {
      var typeCategory = '';
      if(type === 'luz') {
        vm.tableHeaders =  {
          name: 'Luz',
          type: 'LUX'
        };
        typeCategory = 'lux';
      }
      if(type === 'temp') {
        vm.tableHeaders =  {
          name: 'Temperatura',
          type: 'Celsius'
        };
        typeCategory = 'temp';
      }
      if(type === 'gas') {
        vm.tableHeaders =  {
          name: 'Gas',
          type: '%'
        };
        typeCategory = 'gas';
      }
      vm.dataTableList = [];
      for(var j = size - count; j < size; j++) {
        vm.dataTableList.push({
          name: vm.nodeList[data[j].nodeID],
          value: data[j][typeCategory]
        });
      }
      console.log(vm.dataTableList, vm.tableHeaders);
    }

    function configureChart(ctx) {
      vm.chart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: vm.chartConfig.labels,
          datasets: vm.chartConfig.dataSet
        },
        options: {
          response: true,
          maintainAspectRatio: false,
          defaultFontSize: 20,
          responsiveAnimationDuration: 1000,
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:true
              }
            }]
          }
        }
      });
    }

    function init() {
      console.log("Chart start");
      var ctx = document.getElementById("myChart");
      configureChart(ctx);

      /*nodesRequest().get().then(function (response) {
       console.log("patom nodes", response);

       createNodesList(response.data);
       sensorRequest().get().then(function (response) {
       var size = response.data.length;
       createChartData(100, 'luz', response.data, size);

       createTableData(15, 'luz', response.data, size);
       }, function () {
       console.log("Request Error");
       });
       console.log("patom nodes", vm.nodeList);
       }, function () {
       console.log("Request Error");
       });*/
    }

    init();
  }
})();
